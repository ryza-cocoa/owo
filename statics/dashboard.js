jQuery(document).ready(() => {
    let clear_new_resource = (display, dist_id) => {
        $('#resource-dist-id')[0].value = dist_id;
        $('#resource-name')[0].value = "";
        $('#resource-version')[0].value = "";
        $('#resource-file')[0].value = "";
        $($('#dist-resource-uploader-container')[0]).css("display", display);
    };
    let display_resource_table = (resources, dist_id) => {
        // update resource section
        $('#resource-container')[0].innerHTML = '<table id="resource-list" class="table-hover"><thead><tr><th>Name</th><th>With Version</th><th>Action</th></tr></thead><tbody></tbody></table>';
        var resource_table_body = $('<tbody></tbody>')[0];
        resources.forEach(v => {
            var row = $("<tr></tr>")[0];
            var resource_name = $("<td></td>")[0];
            resource_name.innerText = v.resource_name;
            $(resource_name).appendTo(row);

            var version = $("<td></td>")[0];
            if (v.version.length === 0) {
                version.innerText = "Universal";
            } else {
                version.innerText = v.version;
            }
            $(version).appendTo(row);

            var actions = $("<td></td>")[0];
            var delete_btn = $("<a id='delete-dist-resource' data-id='"+dist_id+"' data-ver='"+v.version+"' data-name='"+v.resource_name+"'>Delete</a>")[0];
            $(delete_btn).click(e => {
                e.preventDefault();
                let dist_id = $(e.currentTarget).attr('data-id');
                let resource_version = $(e.currentTarget).attr('data-ver');
                let resource_name = $(e.currentTarget).attr('data-name');
                $.post("/remove-dist-resource", {dist_id: dist_id, resource_version: resource_version, resource_name: resource_name}, d => {
                    location.reload();
                }).fail(() => {
                    alert("error occurred while removing resource");
                });
            });
            $(delete_btn).appendTo(actions);
            $(actions).appendTo(row);
            $(row).appendTo(resource_table_body);
        });

        $($('#resource-list tbody')[0]).empty().replaceWith(resource_table_body);
        $('#resource-list').DataTable();
    };
    let edit_distribution = (display, data, data_id) => {
        $("#dist-editing-container").css("display", display);
        if (data !== null) {
            $.post("/raw-script", { dist_id: data_id, dist_version: data.version }, d => {
                $("#dist-scripts").text(d);
            }).fail(() => {
                alert("Cannot load installation script at the moment x_x");
            });
            $("#dist-id")[0].value = data_id;
            $("#dist-feature-img")[0].src = data.feature_img;
            $("#dist-title")[0].value = data.name;
            $("#dist-short-desc")[0].value = data.short_desc;
            $($("#dist-long-desc")[0]).text(data.long_desc);
            $("#dist-new")[0].value = "no";
            $("#dist-version")[0].value = data.version;
            $("#dist-version").prop( "disabled", true );
            $("#dist-version").prop( "name", "dist-const-version" );
            let hidden = $("<input name='dist_version' type='hidden'>")[0];
            hidden.value = data.version;
            $(hidden).appendTo($("#update-dist-info"));
            $("#dist-resource-uploader").css("display", "block");
            $("#dist-feature-img").css("display", "block");
            display_resource_table(data.resources, data_id);
        } else {
            $("#dist-id")[0].value = "";
            $("#dist-feature-img")[0].src = "";
            $("#dist-title")[0].value = "";
            $("#dist-short-desc")[0].value = "";
            $($("#dist-long-desc")[0]).text("");
            $("#dist-new")[0].value = "yes";
            $("#dist-version")[0].value = "";
            $("#dist-version").prop( "disabled", false );
            $("#dist-resource-uploader").css("display", "none");
            $("#dist-feature-img").css("display", "none");
            display_resource_table([], "");
        }
    };

    $('#software-dist tr').click(e => {
        let data_id = $(e.currentTarget).attr('data-id');
        let data_ver = $(e.currentTarget).attr('data-ver');
        let data = document.owo_data.find((v,i) => {
            return v.id.toString() === data_id && v.version === data_ver;
        });
        if (data === null) return;

        edit_distribution("block", data, data_id);
        clear_new_resource("none");
    });

    $('#software-dist').DataTable();
    $('#resource-list').DataTable();
    $('#add-new-distribution-btn').click(() => {
        edit_distribution("block", null, null);
        clear_new_resource("none", "");
    });
    $('#update-dist-info-btn').click(() => {
        $("#update-dist-info").submit();
    });
    $('#dist-resource-uploader-btn').click(() => {
        clear_new_resource("block", $("#dist-id")[0].value);
    });
    $('#delete-dist').click(e => {
        e.preventDefault();
        let dist_id = $(e.currentTarget).attr('data-id');
        let dist_version = $(e.currentTarget).attr('data-ver');
        $.post("/remove-dist", {dist_id: dist_id, dist_version: dist_version}, d => {
            location.reload();
        }).fail(() => {
            alert("error occurred while removing dist");
        });
    });
});
