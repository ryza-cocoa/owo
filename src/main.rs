use actix_session::{CookieSession, Session};
use actix_web::body::Body;
use actix_web::dev::ServiceResponse;
use actix_web::http::header::{ContentType, Expires};
use actix_web::http::{header, StatusCode};
use actix_web::middleware::errhandlers::{ErrorHandlerResponse, ErrorHandlers};
use actix_web::web::Path;
use actix_web::{web, App, HttpRequest, HttpResponse, HttpServer, Result};
use dirs::home_dir;
use mysql::prelude::*;
use mysql::*;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, Write};
use std::path;
use std::process::exit;
use std::str::FromStr;
use std::time::{Duration, SystemTime};
use templates::statics::StaticFile;
use url::Url;
use actix_multipart::Multipart;
use futures::{StreamExt, TryStreamExt};
extern crate ructe;
#[macro_use]
extern crate lazy_static;
#[macro_use]
mod owo_macro;
mod dao;
use dao::*;

#[derive(Serialize, Deserialize)]
struct Config {
    bind: String,
    mysql: String,
    admin: String,
    username: String,
    password: String,
    site_title: String,
    site_desc: String,
}

#[derive(Deserialize)]
struct SignIn {
    username: String,
    password: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct OwODistribution {
    id: i32,
    name: String,
    feature_img: String,
    short_desc: String,
    long_desc: String,
    downloads: i128,
    pull_url: String,
    push_key: String,
    version: String,
    resources: Vec<OwOResource>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct OwOResource {
    resource_name: String,
    version: String,
}

#[derive(Deserialize)]
struct FormWithDistInfo {
    dist_id: String,
    dist_version: String,
}

#[derive(Deserialize)]
struct FormWithDistResourceInfo {
    dist_id: String,
    resource_version: String,
    resource_name: String,
}

lazy_static! {
    static ref CONFIG: Config = load_config();
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    match test_mysql_connection(&CONFIG.mysql) {
        Ok(_) => (),
        Err(e) => {
            eprintln!("[ERROR] MySQL: {}", e.to_string());
            exit(-1);
        }
    };

    HttpServer::new(move || {
        App::new()
            .wrap(
                ErrorHandlers::new()
                    .handler(StatusCode::NOT_FOUND, render_404)
                    .handler(StatusCode::INTERNAL_SERVER_ERROR, render_500),
            )
            .wrap(CookieSession::signed(&[0; 32]).secure(false))
            // Serve pages
            .service(web::resource("/").to(home_page))
            .service(web::resource(&CONFIG.admin).to(admin))
            .service(web::resource("/signin").to(signin))
            .service(web::resource("/signout").to(signout))

            // Serve files
            .service(web::resource("/static/{filename}").to(static_file))
            .service(web::resource("/feature_img/{filename}").to(feature_img))
            .service(web::resource("/{name}/install.sh").to(install))
            .service(web::resource("/{name}/resources/{resource_name}").to(resource))

            // Serve API endpoints
            .service(web::resource("/update-dist-info").route(web::post().to(update_dist_info)))
            .service(web::resource("/upload-dist-resource").route(web::post().to(upload_dist_resource)))
            .service(web::resource("/raw-script").to(get_raw_script))
            .service(web::resource("/remove-dist").route(web::post().to(remove_dist)))
            .service(web::resource("/remove-dist-resource").route(web::post().to(remove_dist_resource)))

    })
    .bind(&CONFIG.bind)
    .unwrap()
    .run()
    .await
    .expect("Run server");
    Ok(())
}

/// --------------- Serve Pages ---------------

/// Handler for home page.
async fn home_page(param: HttpRequest) -> HttpResponse {
    get_mysql_conn_error_ret!(&CONFIG.mysql, conn, HttpResponse::InternalServerError().finish());
    let conn_info = param.connection_info();
    let prefix = format!("{}://{}", conn_info.scheme(), conn_info.host());
    let available_owo: Vec<(String, String, String, String, String)> = match conn
        .query_map(
            "SELECT id, name, short_desc, long_desc FROM `distribution` ORDER BY `id` DESC",
            |(id, name, short_desc, long_desc)| {
                let id: Option<i32> = id;
                let id = id.unwrap();
                let feature_img = format!("feature_img/{}.png", id);
                let short_desc: Option<String> = short_desc;
                let long_desc: Option<String> = long_desc;
                let curl = format!("bash <(curl -sSLf {}/{}/install.sh)", prefix, name);
                (name, feature_img, short_desc.unwrap_or_default(), long_desc.unwrap_or_default(), curl)
            },
        ) {
        Ok(r) => r,
        Err(e) => {
            let reason = e.to_string();
            eprintln!("[ERROR] query_map: {}", reason.as_str());
            return HttpResponse::InternalServerError()
                .body(reason)
        }
    };
    HttpResponse::Ok().body(render!(templates::index, available_owo.as_ref(), CONFIG.site_title.as_ref(), CONFIG.site_desc.as_ref()))
}

/// Handler for admin page.
async fn admin(session: Session) -> HttpResponse {
    let has_logon = session.get("login").unwrap().unwrap_or(false);
    if has_logon {
        dashboard().await
    } else {
        HttpResponse::Ok().body(render!(templates::signin, &["dummy"]))
    }
}

/// Handler for signin request from admin page.
async fn signin(form: web::Form<SignIn>, session: Session) -> HttpResponse {
    if form.username.eq(&CONFIG.username) && form.password.eq(&CONFIG.password) {
        match session.set("login", true) {
            Ok(()) => {
                HttpResponse::Found()
                    .header("Location", format!("/{}", &CONFIG.admin))
                    .finish()
            },
            Err(e) => {
                eprintln!("[ERROR] Cannot set session: {}", e.to_string());
                HttpResponse::InternalServerError().finish()
            }
        }
    } else {
        let _ = session.set("login", false);
        HttpResponse::Found().header("Location", "/").finish()
    }
}

/// Handler for signout request from admin page.
async fn signout(session: Session) -> HttpResponse {
    let _ = session.set("login", false);
    HttpResponse::Found().header("Location", "/").finish()
}

/// Handler for dashboard content if successfully logon.
async fn dashboard() -> HttpResponse {
    get_mysql_conn_error_ret!(&CONFIG.mysql, conn, HttpResponse::InternalServerError().finish());
    let available_owo: Vec<OwODistribution> = match conn
        .query_map(
            "SELECT `distribution`.`id`, `distribution`.`name`, \
                `distribution`.`short_desc`, `distribution`.`long_desc`, `version`.`downloads`, \
                `distribution`.`pull_url`, `distribution`.`push_key`, `version`.`version` FROM `distribution` \
                LEFT OUTER JOIN `version` ON `version`.`dist_id` = `distribution`.`id` \
                ORDER BY `distribution`.`id` DESC",
            |(id, name, short_desc, long_desc, downloads, pull_url, push_key, version)| {
                let id: Option<i32> = id;
                let id = id.unwrap_or_default();
                let name: Option<String> = name;
                let name = name.unwrap_or("<internal error>".to_string());
                let feature_img = format!("feature_img/{}.png", id);
                let short_desc: Option<String> = short_desc;
                let short_desc= short_desc.unwrap_or_default();
                let long_desc: Option<String> = long_desc;
                let long_desc= long_desc.unwrap_or_default();
                let downloads: Option<i128> = downloads;
                let downloads = downloads.unwrap_or(-1);
                let pull_url: Option<String> = pull_url;
                let pull_url= pull_url.unwrap_or_default();
                let push_key: Option<String> = push_key;
                let push_key= push_key.unwrap_or("<internal error>".to_string());
                let version: Option<String> = version;
                let version= version.unwrap_or_default();
                OwODistribution {
                    id,
                    name,
                    feature_img,
                    short_desc,
                    long_desc,
                    downloads,
                    pull_url,
                    push_key,
                    version,
                    resources: vec![]
                }
            },
        ) {
        Ok(r) => r,
        Err(e) => {
            let reason = e.to_string();
            eprintln!("[ERROR] dashboard query_map: {}", reason.as_str());
            return HttpResponse::InternalServerError()
                .body(reason)
        }
    };

    let available_owo= available_owo.into_iter().map(|mut owo| {
        owo.resources = match conn.exec_map("SELECT `resource_name`, `version` FROM `resource` \
            WHERE `resource`.`dist_id` = ? AND (`resource`.`version` = ? OR `resource`.`version` = '')",
        (owo.id, &owo.version),
        |(resource_name, version)| {
            let resource_name: Option<String> = resource_name;
            let resource_attach_to_ver: Option<String> = version;
            if let Some(resource_name) = resource_name {
                Some(OwOResource {resource_name, version: resource_attach_to_ver.unwrap_or_default()})
            } else {
                None
            }
        }) {
            Ok(ret) => {
                let ret = ret.into_iter().filter(|r| {
                    r.is_some()
                }).map(|r| {
                    r.unwrap()
                }).collect::<Vec<OwOResource>>();
                ret
            },
            Err(e) => {
                let reason = e.to_string();
                eprintln!("[ERROR] dashboard query_map: {}", reason.as_str());
                vec![]
            }
        };
        owo
    }).collect::<Vec<OwODistribution>>();

    let json = serde_json::to_string(&available_owo).unwrap();
    HttpResponse::Ok().body(render!(templates::dashboard, &available_owo, json, "${OWO_RESOURCES_ROOT}", "${OWO_RESOURCES_VER}"))
}

/// --------------- Serve Files ---------------

/// Handler for compile-time static files.
/// Create a response from the file data with a correct content type
/// and a far expires header (or a 404 if the file does not exist).
fn static_file(path: Path<(String,)>) -> HttpResponse {
    let name = &path.0;
    let name = &name.0;

    if let Some(data) = StaticFile::get(name) {
        let far_expires = SystemTime::now() + FAR;
        HttpResponse::Ok()
            .set(Expires(far_expires.into()))
            .set(ContentType(data.mime.clone()))
            .body(data.content)
    } else {
        HttpResponse::NotFound()
            .reason("No such static file.")
            .finish()
    }
}

/// Handler for feature images (non compile-time).
async fn feature_img(path: Path<(String,)>) -> HttpResponse {
    let name = format!("feature_img/{}", &(path.0).0);
    let img_path = path::Path::new(&name);
    if img_path.is_file() {
        if let Ok(mut file) = File::open(img_path) {
            let mut buffer = Vec::new();
            if let Ok(_) = file.read_to_end(&mut buffer) {
                return HttpResponse::Ok()
                    .set(ContentType::png())
                    .body(buffer);
            }
        }
    }

    HttpResponse::NotFound()
        .reason("No such static file")
        .finish()
}

/// Handler for installation scrips (non compile-time).
async fn install(web::Path(name): web::Path<String>, param: HttpRequest) -> HttpResponse {
    let name = name;
    let conn_info = param.connection_info();
    let prefix = format!("{}://{}/{}/resources", conn_info.scheme(), conn_info.host(), &name);

    if let Some(version) = get_version_info_from_req(&param).await {
        if let Some(install_script) = get_scripts_by_name_ver(&CONFIG.mysql, &name, &version).await {
            incr_download_count_by_name_ver(&CONFIG.mysql, &name, &version).await;
            return HttpResponse::Ok()
                .set(ContentType::plaintext())
                .body(install_script.replace("${OWO_RESOURCES_ROOT}", &prefix).replace("${OWO_RESOURCES_VER}", &version).replace("\r\n", "\n"))
        }
    } else {
        if let Some((install_script, latest_version)) = get_latest_scripts_by_name(&CONFIG.mysql, &name).await {
            incr_download_count_by_name_ver(&CONFIG.mysql, &name, &latest_version).await;
            return HttpResponse::Ok()
                .set(ContentType::plaintext())
                .body(install_script.replace("${OWO_RESOURCES_ROOT}", &prefix).replace("${OWO_RESOURCES_VER}", &latest_version).replace("\r\n", "\n"))
        }
    }

    HttpResponse::NotFound()
        .set(ContentType::plaintext())
        .body("# [ERROR] No install script found")
}

/// Handler for resource files (non compile-time).
async fn resource(param: HttpRequest) -> Result<actix_files::NamedFile> {
    let matches = param.match_info();
    let resource_name = if let Some(ver) = get_version_info_from_req(&param).await {
        format!("{}-{}", String::from(matches.get("resource_name").unwrap()), ver)
    } else {
        String::from(matches.get("resource_name").unwrap())
    };
    let dist_name = String::from(matches.get("name").unwrap());
    let resource_path = format!("resources/{}/{}", &dist_name, &resource_name);
    Ok(actix_files::NamedFile::open(resource_path)?)
}

/// --------------- Serve API Endpoints ---------------

async fn get_post_field_data_as_bytes(param: &str, parameter_name: &str, from_field: &mut actix_multipart::Field,
                                      limit_size: usize, move_to: &mut Vec<u8>)
-> Option<HttpResponse> {
    if move_to.len() > 0 { return None; }
    if param.eq(parameter_name) {
        let mut tmp_data = vec![];
        while let Some(chunk) = from_field.next().await {
            let data: actix_web::web::Bytes = chunk.unwrap();
            tmp_data.append(&mut data.to_vec());
            if tmp_data.len() > limit_size {
                return Some(HttpResponse::BadRequest().reason("Data corrupted").finish());
            }
        }
        *move_to = tmp_data;
    }
    None
}

async fn get_post_field_data_as_string(param: &str, parameter_name: &str, from_field: &mut actix_multipart::Field,
                                      limit_size: usize, move_to: &mut String)
                                      -> Option<HttpResponse> {
    if move_to.len() > 0 { return None; }
    let mut tmp_data: Vec<u8> = vec![];
    if let Some(early_err) = get_post_field_data_as_bytes(param, parameter_name, from_field, limit_size, &mut tmp_data).await {
        return Some(early_err);
    }

    let tmp_string = String::from_utf8(tmp_data);
    if let Ok(tmp_string) = tmp_string {
        *move_to = tmp_string;
    } else {
        return Some(HttpResponse::BadRequest().reason("Data corrupted").finish());
    }
    None
}

async fn update_dist_info(mut payload: Multipart, session: Session) -> Result<HttpResponse, actix_web::Error> {
    let has_logon = session.get("login").unwrap().unwrap_or(false);
    if has_logon {
        let mut dist_id: String = "".to_string();
        let mut dist_title: String = "".to_string();
        let mut dist_version: String = "".to_string();
        let mut dist_short_desc: String = "".to_string();
        let mut dist_long_desc: String = "".to_string();
        let mut dist_scripts: String = "".to_string();
        let mut dist_new: String = "".to_string();
        let mut dist_img_data: Vec<u8> = vec![];

        while let Ok(Some(mut field)) = payload.try_next().await {
            if let Some(content_disposition) = field.content_disposition() {
                if let Some(parameter_name) = content_disposition.get_name() {
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "dist_id", &mut field, 11, &mut dist_id).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "dist_title", &mut field, 1024, &mut dist_title).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "dist_version", &mut field, 1024, &mut dist_version).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "dist_short_desc", &mut field, 2048, &mut dist_short_desc).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "dist_long_desc", &mut field, 65536, &mut dist_long_desc).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "dist_scripts", &mut field, 4 * 1024 * 1024, &mut dist_scripts).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "dist_new", &mut field, 4, &mut dist_new).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_bytes(parameter_name, "dist_img", &mut field, 16 * 1024 * 1024, &mut dist_img_data).await {
                        return Ok(early_err);
                    }
                }
            }
        }

        if dist_title.len() == 0 || dist_version.len() == 0 {
            Ok(HttpResponse::BadRequest().reason("Illegal Data").finish())
        } else {
            get_mysql_conn_ok!(&CONFIG.mysql, conn);
            let mut stage1_ok = false;
            let mut is_update = true;
            if dist_new.eq("no") && dist_id.len() > 0 {
                println!("[INFO] update_dist_info: update {}", &dist_id);
                match conn.exec_drop(r"UPDATE `distribution` SET name = ?, short_desc = ?, long_desc = ? WHERE `distribution`.`id` = ?",
                                     (&dist_title, &dist_short_desc, &dist_long_desc, &dist_id)
                ) {
                    Ok(()) => stage1_ok = true,
                    Err(e) => {
                        let msg = e.to_string();
                        eprintln!("[ERROR] update_dist_info: update: stage 1: {}", &msg);
                        return Ok(HttpResponse::Ok().body(msg))
                    }
                };
            } else if dist_new.eq("yes") && dist_id.len() == 0 && dist_version.len() > 0 {
                println!("[INFO] update_dist_info: add {}", &dist_title);
                is_update = false;
                let push_key = generate_push_key();
                match conn.exec_drop(r"INSERT INTO `distribution` (name, short_desc, long_desc, push_key) VALUES(?, ?, ?, ?)",
                                     (&dist_title, &dist_short_desc, &dist_long_desc, &push_key)
                ) {
                    Ok(()) => stage1_ok = true,
                    Err(e) => {
                        let msg = e.to_string();
                        eprintln!("[ERROR] update_dist_info: new: stage 1: {}", &msg);
                        return Ok(HttpResponse::Ok().body(msg))
                    }
                };
            }

            if stage1_ok {
                let mut stage2_ok = false;
                let dist_scripts = dist_scripts.replace("\r\n", "\n");
                if is_update {
                    match conn.exec_drop(r"UPDATE `version` SET `scripts` = ? WHERE `version`.`dist_id` = ? AND `version`.`version` = ?", (&dist_scripts, &dist_id, &dist_version)) {
                        Ok(()) => stage2_ok = true,
                        Err(e) => {
                            let msg = e.to_string();
                            eprintln!("[ERROR] update_dist_info: new: stage 1: {}", &msg);
                            return Ok(HttpResponse::Ok().body(msg))
                        }
                    };
                } else {
                    if let Some(dist_id) = get_id_from_distribution_name(&CONFIG.mysql,&dist_title).await {
                        match conn.exec_drop(r"INSERT INTO `version` (dist_id, version, scripts) VALUES (?, ?, ?)", (&dist_id, &dist_version, &dist_scripts)) {
                            Ok(()) => stage2_ok = true,
                            Err(e) => {
                                let msg = e.to_string();
                                eprintln!("[ERROR] update_dist_info: new: stage 1: {}", &msg);
                                return Ok(HttpResponse::Ok().body(msg))
                            }
                        };
                    } else {
                        eprintln!("[ERROR] update_dist_info: did add {} to db, but cannot find its id", &dist_title);
                    }
                }

                if stage2_ok {
                    if dist_img_data.len() > 0 {
                        let filepath = format!("feature_img/{}.png", &dist_id);
                        let mut f = web::block(|| {
                            std::fs::create_dir_all("feature_img")?;
                            std::fs::File::create(filepath)
                        })
                            .await
                            .unwrap();
                        let _ = web::block(move || f.write_all(&dist_img_data).map(|_| f)).await?;
                    }
                }
            }
            Ok(HttpResponse::Found().header("Location", format!("/{}", &CONFIG.admin)).finish())
        }
    } else {
        Ok(HttpResponse::Forbidden().reason("Not login").finish())
    }
}

async fn upload_dist_resource(mut payload: Multipart, session: Session) -> Result<HttpResponse, actix_web::Error> {
    let has_logon = session.get("login").unwrap().unwrap_or(false);
    if has_logon {
        let mut resource_dist_id: String = "".to_string();
        let mut resource_name: String = "".to_string();
        let mut resource_version: String = "".to_string();
        let mut resource_data: Vec<u8> = vec![];

        while let Ok(Some(mut field)) = payload.try_next().await {
            if let Some(content_disposition) = field.content_disposition() {
                if let Some(parameter_name) = content_disposition.get_name() {
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "resource_dist_id", &mut field, 11, &mut resource_dist_id).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "resource_name", &mut field, 512, &mut resource_name).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_string(parameter_name, "resource_version", &mut field, 1024, &mut resource_version).await {
                        return Ok(early_err);
                    }
                    if let Some(early_err) = get_post_field_data_as_bytes(parameter_name, "resource_file", &mut field, 256 * 1024 * 1024, &mut resource_data).await {
                        return Ok(early_err);
                    }
                }
            }
        }

        if resource_dist_id.len() == 0 || resource_name.len() == 0 || resource_data.len() == 0 {
            Ok(HttpResponse::BadRequest().reason("Illegal Data").finish())
        } else {
            get_mysql_conn_ok!(&CONFIG.mysql, conn);
            let resource_name = sanitize_filename::sanitize(resource_name);
            let resource_dist_id = match i32::from_str(resource_dist_id.as_ref()) {
                Ok(resource_dist_id) => resource_dist_id,
                Err(_) => return Ok(HttpResponse::BadRequest().reason("Illegal Data").finish())
            };
            if let Some(name) = get_name_from_distribution_id(&CONFIG.mysql, &resource_dist_id).await {
                match conn.exec_drop(r"INSERT INTO `resource` (`dist_id`, `resource_name`, `version`) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE `resource`.`version` = ?",
                                     (&resource_dist_id, &resource_name, &resource_version, &resource_version)
                ) {
                    Ok(()) => {
                        let filepath = if resource_version.len() > 0 {
                            format!("resources/{}/{}-{}", &name, &resource_name, &resource_version)
                        } else {
                            format!("resources/{}/{}", &name, &resource_name)
                        };
                        let cname = name.clone();
                        let cresource_name = resource_name.clone();
                        let mut f = match web::block(move || {
                            std::fs::create_dir_all(format!("resources/{}", &name))?;
                            std::fs::File::create(filepath)
                        }).await {
                            Ok(f) => f,
                            Err(_e) => {
                                eprintln!("[ERROR] Cannot create dir or file while updating resources {} for {}", cresource_name, cname);
                                return Ok(HttpResponse::InternalServerError().finish())
                            }
                        };
                        let _ = web::block(move || f.write_all(&resource_data).map(|_| f)).await?;
                        Ok(HttpResponse::Found().header("Location", format!("/{}", &CONFIG.admin)).finish())
                    },
                    Err(e) => Ok(HttpResponse::Ok().body(e.to_string()))
                }
            } else {
                Ok(HttpResponse::BadRequest().reason("Illegal Data").finish())
            }
        }
    } else {
        Ok(HttpResponse::Forbidden().reason("Not login").finish())
    }
}

async fn get_raw_script(form: web::Form<FormWithDistInfo>, session: Session) -> HttpResponse {
    let has_logon = session.get("login").unwrap().unwrap_or(false);
    if has_logon {
        string_to_i32_error_ret!(form.dist_id, id, HttpResponse::Forbidden().reason("Bad id").finish());
        let scripts= get_scripts_by_id_ver(&CONFIG.mysql, &id, &form.dist_version).await.unwrap_or_default();
        HttpResponse::Ok().body(scripts)
    } else {
        HttpResponse::Forbidden().reason("Not login").finish()
    }
}

async fn remove_dist(form: web::Form<FormWithDistInfo>, session: Session) -> HttpResponse {
    let has_logon = session.get("login").unwrap().unwrap_or(false);
    if has_logon {
        string_to_i32_error_ret!(form.dist_id, id, HttpResponse::Forbidden().reason("Bad id").finish());
        let ret = remove_dist_by_id_ver(&CONFIG.mysql, &id, &form.dist_version).await;
        if ret == 0 {
            if let Some(name) = get_name_from_distribution_id(&CONFIG.mysql, &id).await {
                let _ = web::block(move || {
                    std::fs::remove_dir_all(format!("resources/{}", &name))?;
                    std::fs::remove_dir_all(format!("scripts/{}", &name))?;
                    std::fs::remove_file(format!("feature_img/{}.png", &id))
                }).await;
            }
            HttpResponse::Ok().body("ok")
        } else if ret < 0 {
            HttpResponse::InternalServerError().finish()
        } else {
            HttpResponse::Forbidden().reason("Not login").finish()
        }
    } else {
        HttpResponse::Forbidden().reason("Not login").finish()
    }
}

async fn remove_dist_resource(form: web::Form<FormWithDistResourceInfo>, session: Session) -> HttpResponse {
    let has_logon = session.get("login").unwrap().unwrap_or(false);
    if has_logon {
        string_to_i32_error_ret!(form.dist_id, id, HttpResponse::Forbidden().reason("Bad id").finish());

        if remove_dist_resource_by_id_ver(&CONFIG.mysql, &id, &form.resource_version, &form.resource_name).await {
            let filepath = if let Some(name) = get_name_from_distribution_id(&CONFIG.mysql, &id).await {
                Some(if form.resource_version.len() > 0 {
                    format!("resources/{}/{}-{}", &name, &form.resource_name, &form.resource_version)
                } else {
                    format!("resources/{}/{}", &name, &form.resource_name)
                })
            } else {
                None
            };

            if let Some(filepath) = filepath {
                let _ = web::block(|| {
                    std::fs::remove_file(filepath)
                }).await;
            }

            HttpResponse::Ok().body("ok")
        } else {
            HttpResponse::InternalServerError().finish()
        }
    } else {
        HttpResponse::Forbidden().reason("Not login").finish()
    }
}

/// --------------- Helper Functions ---------------

async fn get_version_info_from_req(param: &HttpRequest) -> Option<String> {
    let conn_info = param.connection_info();
    let prefix = format!("{}://{}", conn_info.scheme(), conn_info.host());
    let full_req_url = format!("{}?{}", &prefix, param.query_string());

    let url = match Url::parse(full_req_url.as_ref()) {
        Ok(url) => url,
        Err(_e) => {
            eprintln!("[ERROR] get_version_info_from_req: Cannot parse the request URI: {}", full_req_url);
            return None;
        }
    };
    let mut pairs = url.query_pairs();
    let num = pairs.count();
    let mut ver: Option<String> = None;
    for _ in 0..num {
        let (key, val) = pairs.next().unwrap();
        if key.eq("ver") {
            ver = Some(val.into_owned());
            break;
        }
    }
    ver
}

fn render_404(res: ServiceResponse<Body>) -> Result<ErrorHandlerResponse<Body>> {
    error_response(
        res,
        StatusCode::NOT_FOUND,
        "The resource you requested can't be found.".to_owned(),
    )
}

fn render_500(res: ServiceResponse<Body>) -> Result<ErrorHandlerResponse<Body>> {
    error_response(
        res,
        StatusCode::INTERNAL_SERVER_ERROR,
        "Sorry, Something went wrong. This is probably not your fault.".to_owned(),
    )
}

fn error_response(
    mut res: ServiceResponse<Body>,
    status_code: StatusCode,
    message: String,
) -> Result<ErrorHandlerResponse<Body>> {
    res.headers_mut().insert(
        header::CONTENT_TYPE,
        header::HeaderValue::from_str(mime::TEXT_HTML_UTF_8.as_ref()).unwrap(),
    );
    Ok(ErrorHandlerResponse::Response(res.map_body(
        |_head, _body| {
            actix_web::dev::ResponseBody::Body(
                render!(templates::error, status_code, &message).into(),
            )
        },
    )))
}

fn generate_push_key() -> String {
    use rand::Rng;
    const CHARSET: &[u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ\
                            abcdefghijklmnopqrstuvwxyz\
                            0123456789";
    const PUSH_KEY_LEN: usize = 12;
    let mut rng = rand::thread_rng();

    let push_key: String = (0..PUSH_KEY_LEN)
        .map(|_| {
            let idx = rng.gen_range(0, CHARSET.len());
            CHARSET[idx] as char
        })
        .collect();
    push_key
}

fn load_config() -> Config {
    let mut home_path = match home_dir() {
        Some(h) => h,
        None => {
            eprintln!("[ERROR] Cannot find $HOME");
            exit(-1);
        }
    };
    home_path.push(".owo-dist.json");
    let file = match File::open(&home_path) {
        Ok(f) => f,
        Err(e) => {
            eprintln!(
                "[ERROR] Cannot open ~/.owo-dist.json, reason: {}",
                e.to_string()
            );
            exit(-1);
        }
    };
    let reader = BufReader::new(file);
    match serde_json::from_reader(reader) {
        Ok(c) => c,
        Err(e) => {
            eprintln!("[ERROR] Cannot parse ~/.owo-dist.json: {}", e.to_string());
            exit(-1);
        }
    }
}

/// A duration to add to current time for a far expires header.
static FAR: Duration = Duration::from_secs(180 * 24 * 60 * 60);

// And finally, include the generated code for templates and static files.
include!(concat!(env!("OUT_DIR"), "/templates.rs"));
