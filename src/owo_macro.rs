macro_rules! get_mysql_conn_error_ret {
    ($mysql_url: expr, $conn: ident, $ret: expr) => {
        let pool = match Pool::new($mysql_url) {
            Ok(pool) => pool,
            Err(e) => {
                let reason = e.to_string();
                eprintln!("[ERROR] Pool::new: {}", reason.as_str());
                return $ret;
            }
        };
        #[allow(unused_mut)]
        #[allow(unused_variables)]
        let mut $conn = match pool.get_conn() {
            Ok(conn) => conn,
            Err(e) => {
                let reason = e.to_string();
                eprintln!("[ERROR] pool::get_conn: {}", reason.as_str());
                return $ret;
            }
        };
    };
}

macro_rules! get_mysql_conn_ok {
    ($mysql_url: expr, $conn: ident) => {
        let pool = match Pool::new($mysql_url) {
            Ok(pool) => pool,
            Err(e) => {
                let reason = e.to_string();
                eprintln!("[ERROR] Pool::new: {}", reason.as_str());
                return Ok(HttpResponse::InternalServerError().body(reason));
            }
        };
        let mut $conn = match pool.get_conn() {
            Ok(conn) => conn,
            Err(e) => {
                let reason = e.to_string();
                eprintln!("[ERROR] pool::get_conn: {}", reason.as_str());
                return Ok(HttpResponse::InternalServerError().body(&reason));
            }
        };
    };
}

macro_rules! string_to_i32_error_ret {
    ($string: expr, $ret: ident, $error_ret: expr) => {
        let $ret = match i32::from_str($string.as_ref()) {
            Ok(id) => id,
            Err(_) => return $error_ret
        };
    };
}

use std::io::Write;

#[macro_export]
macro_rules! render {
    ($template:path) => (Render(|o| $template(o)));
    ($template:path, $($arg:expr),*) => {{
        use owo_macro::Render;
        Render(|o| $template(o, $($arg),*))
    }};
    ($template:path, $($arg:expr),* ,) => {{
        use owo_macro::Render;
        Render(|o| $template(o, $($arg),*))
    }};
}

pub struct Render<T: FnOnce(&mut dyn Write) -> std::io::Result<()>>(pub T);

impl<T: FnOnce(&mut dyn Write) -> std::io::Result<()>> From<Render<T>>
for actix_web::body::Body
{
    fn from(t: Render<T>) -> Self {
        let mut buf = Vec::new();
        match t.0(&mut buf) {
            Ok(()) => buf.into(),
            Err(_e) => {
                //log::warn!("Failed to render: {}", e);
                "Render failed".into()
            }
        }
    }
}
