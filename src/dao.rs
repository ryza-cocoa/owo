use mysql::prelude::*;
use mysql::*;

pub fn test_mysql_connection(mysql_url: &String) -> mysql::Result<()> {
    let pool = Pool::new(mysql_url)?;
    let mut conn = pool.get_conn()?;
    conn.query_drop(r"CREATE TABLE IF NOT EXISTS `distribution` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(512) NOT NULL UNIQUE KEY,
                `feature_img` VARCHAR(128),
                `short_desc` VARCHAR(256),
                `long_desc` TEXT,
                `downloads` BIGINT DEFAULT 0,
                `pull_url` TEXT,
                `push_key` CHAR(16) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;",
    )?;

    conn.query_drop(r"CREATE TABLE IF NOT EXISTS `version` (
                `dist_id` INT,
                `version` VARCHAR(256) NOT NULL,
                `update_time` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `downloads` BIGINT DEFAULT 0,
                `scripts` TEXT,
                FOREIGN KEY (`dist_id`) REFERENCES `distribution` (`id`) ON DELETE CASCADE,
                UNIQUE INDEX (`dist_id`, `version`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;",
    )?;

    conn.query_drop(r"CREATE TABLE IF NOT EXISTS `resource` (
                `dist_id` INT,
                `version` VARCHAR(256),
                `resource_name` VARCHAR(128) NOT NULL,
                FOREIGN KEY (`dist_id`) REFERENCES `distribution` (`id`) ON DELETE CASCADE,
                UNIQUE INDEX (`dist_id`, `resource_name`, `version`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;",
    )?;
    Ok(())
}

pub async fn get_name_from_distribution_id(mysql_url: &String, id: &i32) -> Option<String> {
    get_mysql_conn_error_ret!(mysql_url, conn, None);
    match conn.exec_first::<String, &str, (&i32,)>(r"SELECT name FROM `distribution` WHERE `distribution`.`id` = ?", (id,)) {
        Ok(name) => name,
        Err(e) => {
            eprintln!("[ERROR] get_name_from_distribution_id: {}", e.to_string());
            None
        }
    }
}

pub async fn get_id_from_distribution_name(mysql_url: &String, name: &String) -> Option<i32> {
    get_mysql_conn_error_ret!(mysql_url, conn, None);
    match conn.exec_first::<i32, &str, (&String,)>(r"SELECT id FROM `distribution` WHERE `distribution`.`name` = ?", (name,)) {
        Ok(id) => id,
        Err(e) => {
            eprintln!("[ERROR] get_id_from_distribution_name: {}", e.to_string());
            None
        }
    }
}

pub async fn get_scripts_by_id_ver(mysql_url: &String, id: &i32, version: &String) -> Option<String> {
    get_mysql_conn_error_ret!(mysql_url, conn, None);
    match conn.exec_first::<String, &str, (&i32, &String,)>(r"SELECT scripts FROM `version` WHERE `version`.`dist_id` = ? AND `version`.`version` = ?", (id, version)) {
        Ok(scripts) => scripts,
        Err(e) => {
            eprintln!("[ERROR] get_scripts_by_id_ver: {}", e.to_string());
            None
        }
    }
}

pub async fn get_scripts_by_name_ver(mysql_url: &String, name: &String, version: &String) -> Option<String> {
    get_mysql_conn_error_ret!(mysql_url, conn, None);
    if let Some(id) = get_id_from_distribution_name(mysql_url, name).await {
        get_scripts_by_id_ver(mysql_url, &id, version).await
    } else {
        None
    }
}

pub async fn get_latest_scripts_by_name(mysql_url: &String, name: &String) -> Option<(String, String)> {
    get_mysql_conn_error_ret!(mysql_url, conn, None);
    if let Some(id) = get_id_from_distribution_name(mysql_url, name).await {
        match conn.exec_first::<(String, String), &str, (&i32,)>(r"SELECT `version`.`scripts`, `version`.`version` FROM `version` WHERE `version`.`dist_id` = ? ORDER BY `version`.`version` DESC LIMIT 1", (&id,)) {
            Ok(scripts_version) => scripts_version,
            Err(e) => {
                eprintln!("[ERROR] get_latest_scripts_by_name: {}", e.to_string());
                None
            }
        }
    } else {
        None
    }
}

pub async fn incr_download_count_by_name_ver(mysql_url: &String, name: &String, version: &String) {
    get_mysql_conn_error_ret!(mysql_url, conn, ());
    if let Some(id) = get_id_from_distribution_name(mysql_url, name).await {
        match conn.exec_drop(r"UPDATE `version` SET `version`.`downloads` = `version`.`downloads` + 1 WHERE `version`.`dist_id` = ? AND `version`.`version` = ?", (&id, &version, )) {
            Ok(_) => (),
            Err(e) => eprintln!("[ERROR] incr_download_count_by_name_ver: {}", e.to_string())
        };
    }
}

pub async fn remove_dist_by_id_ver(mysql_url: &String, id: &i32, version: &String) -> i32 {
    get_mysql_conn_error_ret!(mysql_url, conn, -1);
    let mut ok = false;
    let mut ret = -1i32;
    match conn.exec_drop(r"DELETE FROM `version` WHERE `version`.`dist_id` = ? AND `version`.`version` = ?", (&id, &version)) {
        Ok(()) => {
            match conn.exec_first::<i32, &str, (&i32,)>(r"SELECT COUNT(*) FROM `version` WHERE `version`.`dist_id` = ?", (&id,)) {
                Ok(count) => {
                    ret = count.unwrap();
                    if ret == 0 {
                        match conn.exec_drop(r"DELETE FROM `distribution` WHERE `distribution`.`id` = ?", (&id,)) {
                            Ok(()) => ok = true,
                            Err(e) => eprintln!("[ERROR] error while delete all {}: {}", &id, e.to_string())
                        };
                    } else {
                        ok = true;
                    }
                },
                Err(e) => eprintln!("[ERROR] error while query version count for {}: {}", &id, e.to_string())
            }
        },
        Err(e) => eprintln!("[ERROR] get_latest_scripts_by_name: {}", e.to_string()),
    }

    if ok { ret }
    else { -1 }
}

pub async fn remove_dist_resource_by_id_ver(mysql_url: &String, id: &i32, version: &String, name: &String) -> bool {
    get_mysql_conn_error_ret!(mysql_url, conn, false);
    match conn.exec_drop(r"DELETE FROM `resource` WHERE `resource`.`dist_id` = ? AND `resource`.`version` = ? AND `resource`.`resource_name` = ?", (&id, &version, &name)) {
        Ok(()) => true,
        Err(e) => {
            eprintln!("[ERROR] remove_dist_resource_by_id_ver: {}", e.to_string());
            false
        }
    }
}
