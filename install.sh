#/bin/sh

function error {
    msg=$1;
    echo "[ERROR] ${msg}"
}

function info {
    msg=$1;
    echo "[INFO] ${msg}"
}

function download {
    url=$1
    save_at=$2
    
    CURL=`which curl`
    if [ ${#CURL} -eq 0 ]; then
        error "Cannot find curl, give up"
        exit
    fi
    
    curl -fSsL ${url} -o ${save_at}
}

function yes_or_no {
    read ans
    case $ans in
        [yY])
            echo 1
        ;;
        *)
            echo 0
        ;;
    esac
}

function non_empty_ans {
    while true; do
        read ans
        if [ ${#ans} -gt 0 ]; then
            echo ${ans}
            break
        fi
    done
}

function ans_with_default {
    d=$1
    read ans
    if [ ${#ans} -gt 0 ]; then
        echo $ans
    else
        echo $d
    fi
}

OS_TYPE=""
ARCH="x86_64"
PRE_COMPILED="owo"
case `uname` in
    [Ll]inux)
        OS_TYPE="linux"
        CUR_ARCH=`uname -i`
        if [ $CUR_ARCH != "x86_64" ]; then
            error "Currently only has x86_64 precompiled"
            exit
        fi
        PRE_COMPILED="owo-${OS_TYPE}-${CUR_ARCH}"
    ;;
    [Dd]arwin)
        OS_TYPE="macos"
        PRE_COMPILED="owo-${OS_TYPE}-${ARCH}"
    ;;
    *)
        error "Currently no support for your system"
        exit
    ;;
esac

download "${OWO_RESOURCES_ROOT}/${PRE_COMPILED}?ver=${OWO_RESOURCES_VER}" "/usr/local/bin/owo"
if [ $? -eq 0 ]; then
    chmod +x /usr/local/bin/owo
    info "Successfully saved owo binary to /usr/local/bin/owo"
    
    SYSTEMCTL=`which systemctl`
    if [ ${#SYSTEMCTL} -ne 0 ]; then
        echo -n "Would you like to download systemd service file?[yN]: "
        ans=$( yes_or_no )
        if [ $ans -eq 0 ]; then
            info "Found systemctl, will download owo.service to /etc/systemd/system/owo.service."
            download "${OWO_RESOURCES_ROOT}/owo.service?ver=${OWO_RESOURCES_VER}" "/etc/systemd/system/owo.service"
            if [ $? -eq 0 ]; then
                info "Successfully saved systemd service file to /etc/systemd/system/owo.service"
            else
                error "Cannot download or save systemd service file"
            fi
        fi
    fi

    echo -n "Would you like to config owo now? [yN] "
    ans=$( yes_or_no )
    if [ $ans -eq 1 ]; then
        echo -n "Listen interface: [0.0.0.0:8088]: "
        bind=$(ans_with_default "0.0.0.0:8088")
        
        echo -n "MySQL address: [127.0.0.1:3306]: "
        mysql_address=$(ans_with_default "127.0.0.1:3306")
        
        echo -n "MySQL username: "
        mysql_username=$(non_empty_ans)
        
        echo -n "MySQL password: "
        mysql_password=$(non_empty_ans)
        
        echo -n "MySQL database name: [owo]: "
        database=$(ans_with_default "owo")
        
        echo -n "admin URI: [admin]: "
        admin=$(ans_with_default "admin")
        
        echo -n "admin username: [root]: "
        admin_username=$(ans_with_default "root")
        
        echo -n "admin password: "
        admin_password=$(non_empty_ans)
        
        echo -n "site title: [OwO]: "
        site_title=$(ans_with_default "OwO")
        
        echo -n "site description: []: "
        site_desc=$(ans_with_default "")
        
        cat <<EOF | tee "~/.owo-dist.json"
{
    "bind": "$bind",
    "mysql": "mysql://${mysql_username}:${mysql_password}@${mysql_address}/${database}",
    "admin": "${admin}",
    "username": "${admin_username}",
    "password": "${admin_password}",
    "site_title": "${site_title}",
    "site_desc": "${site_desc}"
}
EOF
    else
        echo -n "Would you like to download an example config file? [yN] "
        ans=$( yes_or_no )
        if [ $ans -eq 1 ]; then
            overwrite=1
            if [ -e "~/.owo-dist.json" ]; then
                echo -n "You already has a config file, overwrite? [yN] "
                ans=$( yes_or_no )
                overwrite=$ans
            fi
            if [ $overwrite -eq 1 ]; then
                info "~/.owo-dist.json not exists, will download an example config file to ~/.owo-dist.json"
                download "${OWO_RESOURCES_ROOT}/owo-dist.json?ver=${OWO_RESOURCES_VER}" "~/.owo-dist.json"
                if [ $? -eq 0 ]; then
                    info "Successfully saved example config file to ~/.owo-dist.json"
                else
                    error "Cannot download or save example config file"
                fi
            fi
        fi
    fi
fi
