# owo

The OwO software distribution system!

![screenshot](screenshot.png)

## Dependency
- MySQL, required
- nginx, recommended
- OpenSSL devel & pkg-config, if you're using Linux & you want to compile this by yourself

## Usage
1. Setup database
```bash
# Login to your MySQL server and create a database
# Let's suppose your MySQL username is root and password is alpine
mysql -uroot -p alpine

# Then create a database, let's say owo
mysql> CREATE DATABASE owo;

# If no error shows, you can exit MySQL now
mysql> exit
```

2. Install owo
There will be a detailed prompt while installation.
```bash
# this script will save owo to /usr/local/bin/owo
# and create a example config file at ~/.owo-dist.json
# and generate a systemd service file at /etc/systemd/system/owo.service if your system is Ubuntu/Debian
curl -fSsL https://owo.ryza.moe/owo/install.sh | sudo bash
```

3. [Optional] Or manually install owo

Firstly, download precompiled binary or manually compile this project. The following content will assume that `owo` is placed at `/usr/local/bin` (or any other location in $PATH).

Then copy `owo-dist.example.json` to `~/.owo-dist.json`, also make sure it's readable by `owo`.

4. Config OwO

Now you can edit `~/.owo-dist.json`. 
```json
{
    "bind": "0.0.0.0:8088",                                  # IP:Port that owo serves on
    "mysql": "mysql://root:alpinemoe@localhost:3306/owo",    # MySQL connection URL
    "admin": "lovelive",                                     # admin page URI
    "username": "root",                                      # admin username
    "password": "alpine",                                    # admin password
    "site_title": "Ryza OwO",                                # site title
    "site_desc": "大概是有趣的東西們"                           # site description
}
```

4. Start OwO
```bash
# Ubuntu / Debian
sudo systemctl enable owo.service
sudo systemctl start owo.service

# Other
# You can write something similar by yourself
```
